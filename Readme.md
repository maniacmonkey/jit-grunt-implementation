#Dummy Grunt Project

#####Grunt Stack

Project uses following grunt stack

 - [oad-grunt-configs](https://github.com/creynders/load-grunt-configs)
 - [it-gruntj(https://www.npmjs.com/package/jit-grunt)
 - [browserify](https://github.com/jmreidy/grunt-browserify)
 - [babelify](https://github.com/babel/babelify)
 - [livereload](https://github.com/gruntjs/grunt-contrib-livereload)
 - [connect](https://github.com/gruntjs/grunt-contrib-connect)



####Help
- [ECMAScript 6 Help](https://www.sitepoint.com/?s=ecmascript+6)
- [Babel Options](http://babeljs.io/docs/usage/options/)
- [Underscore vs ES6](https://www.reindex.io/blog/you-might-not-need-underscore/)






